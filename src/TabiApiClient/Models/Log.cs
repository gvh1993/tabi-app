﻿using System;
namespace TabiApiClient.Models
{
    public class Log
    {
        public string Origin { get; set; }
        public string Event { get; set; }
        public string Message { get; set; }
    }
}
