﻿using System;
namespace Tabi.Helpers
{
    public interface IVersion
    {
        string GetVersion();
    }
}
